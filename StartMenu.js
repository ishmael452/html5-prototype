LightRunner.StartMenu = function(game) 
{
    this.startBG;
    this.startPrompt;
    this.ding;
    this.text;
    this.style;
    
}

LightRunner.StartMenu.prototype = 
{
	
	create: function() 
    {
       // this.ding = this.add.audio('select_audio');
        
		startBG = this.add.image(0, 0, 'titlescreen');
		startBG.inputEnabled = true;
		startBG.events.onInputDown.addOnce(this.startGame, this);
        
        text = "START.";
        style = { font: "40px Arial", fill: "#FF6801", align: "center" };

        var t = this.add.text(this.world.centerX-70, this.world.centerY-140, text, style);
		
	//	startPrompt = this.add.bitmapText(this.world.centerX-155, this.world.centerY+180, 'eightbitwonder', 'Touch to Start!', 24);
	},

	startGame: function(pointer) 
    {
       // this.ding.play();
		this.state.start('Game');
	}
};