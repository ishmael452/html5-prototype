LightRunner.Game = function(game)
{
    this.screenHeight=900;
    this.screenWidth=550;
    this. player;
    this. cursors;
    this. isPressed = false;
    this. lineSection=null;
    this. linePath = new Array();
    this. spacer = 1;
    this. clear_x=0;
    this. clear_y=0;
    this. rotate_line=0;
    this. blueLineColor = true;
    this. lineColorChangeTimer = 0;
    this. bg1;
    this. bg2;
    this. StaticLengthLine = true;
    this.bgScrollSpeed = 5;
    this.counter=0;
    this.numSegments = 128;
    this.speed=550;
    this.enemy;
    this.numLasers = 2;
    this.laser1 = new Array();
    this.laser2 = new Array();
    this.laser3 = new Array();
    this.b1;
    this.b2;
    this.b3;
    this.updateSpeed=4;
    this.laserSpeed = 150;
    //this.moveSpeed = 5;
}


LightRunner.Game.prototype = {

create: function ()
{
    
    this.physics.startSystem(Phaser.Physics.ARCADE);
    bg1 = this.add.sprite(0, 0, 'background');
    bg2 = this.add.sprite(0, -this.screenHeight, 'background');
   
	this.lineSection = this.add.group();
    this.lineSection.enableBody = true;
    this.lineSection.physicsBodyType = Phaser.Physics.ARCADE;
	
	this.ChangeLineSprite();//*/
   /* for(var i = 0; i < numSections*spacer; i++)
	{
		linePath[i] = new Phaser.Point(game.world.centerX,400);
	}//*/
	

    this.player = this.add.sprite(this.world.centerX,400,'player','character.png');
    this.physics.enable(this.player, Phaser.Physics.ARCADE);
   for(var i=0; i < this.numSegments; i++)
	{
		this.lineSection.create(this.player.x+11*this.player.width/32,this.player.y + 7*this.player.height/8,'trail');
      //  this.physics.enable(this.lineSection.getAt(i),Phaser.Physics.ARCADE);
        
	}
    this.physics.enable(this.lineSection,Phaser.Physics.ARCADE);
    this.player.body.collideWorldBounds = true;
    this.player.body.bounce.set(1);
    this.player.body.immovable = true;
    this.clear_x = this.player.x+11*this.player.width/32;
    this.clear_y = this.player.y + 7*this.player.height/8;
    cursors = this.input.keyboard.createCursorKeys();
    
    this.CreateEnemy();

},
    
CreateEnemy:  function ()
{
	
	enemy = this.add.sprite(0, this.screenHeight-150, 'enemy');
	
    for(var i=0; i<this.numLasers; i++){
        this.laser1[i] = this.add.sprite(100, this.screenHeight-150, 'bLaser');  
        this.physics.enable(this.laser1[i], Phaser.Physics.ARCADE);
        this.laser1[i].body.velocity.setTo(0, -this.laserSpeed);
        this.laser1[i].checkWorldBounds = true;
        this.laser1[i].outOfBoundsKill = true;
        
        this.laser2[i] = this.add.sprite(260, this.screenHeight-150, 'bLaser');  
        this.physics.enable(this.laser2[i], Phaser.Physics.ARCADE);
        this.laser2[i].body.velocity.setTo(0, -this.laserSpeed);
        this.laser2[i].checkWorldBounds = true;
        this.laser2[i].outOfBoundsKill = true;
        
        this.laser3[i] = this.add.sprite(410, this.screenHeight-150, 'bLaser');  
        this.physics.enable(this.laser3[i], Phaser.Physics.ARCADE);
        this.laser3[i].body.velocity.setTo(0, -this.laserSpeed);
        this.laser3[i].checkWorldBounds = true;
        this.laser3[i].outOfBoundsKill = true;
    }
	
	b1 = Math.floor((Math.random() * 100) + 30); // random number between 1 and 10
    b2 = Math.floor((Math.random() * 100) + 30);
    b3 = Math.floor((Math.random() * 100) + 30);
},

playerHitLine: function(_player,_line)
{
	//player.x = this.world.centerX;
	//player.y = 32;
	//_player.kill();
	//this.paused = true;
	//cursors = null;
},

update: function () 
{

    
	if(this.paused == true)
	   return;
    //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
    //this.physics.arcade.overlap(player, stars, collectStar, null, this);

    //  Reset the players velocity (movement)
    this.UpdateEnemy();
    this.MoveBackground(bg1);
    this.MoveBackground(bg2);
    
    this.player.body.velocity.setTo(0,0);
    //player.anchor.setTo(0.5,0.5);
    this.physics.arcade.enable(this.player);
   // this.physics.arcade.overlap(player,this.lineSection,playerHitLine,null,true);
    this.player.body.bounce.y = 0.2;
    this.player.body.gravity.y = 0;
    this.player.body.collideWorldBounds = true;
    
  
	if(this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.lineColorChangeTimer==0)
	{
		this.lineColorChangeTimer = 200;
		this.blueLineColor = !this.blueLineColor;
		this.ChangeLineSprite();
        
	}

        /*this.physics.arcade.overlap(this.laser1, this.lineSection, this.LaserHitLine, null, true);
        this.physics.arcade.overlap(this.laser2, this.lineSection, this.LaserHitLine, null, true);
        this.physics.arcade.overlap(this.laser3, this.lineSection, this.LaserHitLine, null, true);*/
    for(var i=0;i<this.numLasers;i++)
    {
	//	this.physics.arcade.collide(player,this.lineSection[i],playerHitLine,null,true);
        /*this.physics.arcade.collide(this.laser1, this.lineSection.getAt(i), this.LaserHitLine, null, this);
        this.physics.arcade.overlap(this.laser2, this.lineSection.getAt(i), this.LaserHitLine, null, this);
        this.physics.arcade.overlap(this.laser3, this.lineSection.getAt(i), this.LaserHitLine, null, this);*/
        this.physics.arcade.overlap(this.laser1[i], this.lineSection, this.LaserHitLine, null, this);
        this.physics.arcade.overlap(this.laser2[i], this.lineSection, this.LaserHitLine, null, this);
        this.physics.arcade.overlap(this.laser3[i], this.lineSection, this.LaserHitLine, null, this);
        
    }//*/
	//this.physics.arcade.collide(player,this.lineSection, playerHitLine,null,this);
    if (cursors.left.isDown)
    {
        //  Move to the left
        this.player.body.velocity.x = -this.speed;
		this.clear_x = this.player.x+11*this.player.width/32;
		this.clear_y = this.player.y + 7*this.player.height/8;
		rotate_line = 90;
        this.counter = 0;
		if(this.StaticLengthLine == false)
			this.UpdateLineLeftRight('l');
		else
			this.UpdateStaticLength();
       // player.animations.play('left');
    }
    else if (cursors.right.isDown)
    {
        //  Move to the right
        this.player.body.velocity.x = this.speed;
		this.clear_x = this.player.x+11*this.player.width/32;
		this.clear_y = this.player.y + 7*this.player.height/8;
		rotate_line = 90;
        this.counter = 0;
		if(this.StaticLengthLine == false)
			this.UpdateLineLeftRight('r');
		else
			this.UpdateStaticLength();
        //player.animations.play('right');
    }
    
    //  Allow the player to jump if they are touching the ground.
    else if (cursors.up.isDown)
    {
        this.player.body.velocity.y = -this.speed;
		this.clear_x = this.player.x+11*this.player.width/32;
		this.clear_y = this.player.y + 7*this.player.height/8;
		rotate_line = 180;
        this.counter = 0;
		if(this.StaticLengthLine == false)
        {
			this.UpdateLineUpDown('u');
        }
		else
        {
			this.UpdateStaticLength();
        }
    }
	else if (cursors.down.isDown)
	{
		this.player.body.velocity.y = this.speed;
		this.clear_x = this.player.x+11*this.player.width/32;
		this.clear_y = this.player.y + 7*this.player.height/8;
		rotate_line = 180;
        this.counter = 0;
		if(this.StaticLengthLine == false)
        {
			this.UpdateLineUpDown('d');
        }
		else
		{
			this.UpdateStaticLength();
		}
	}
	else
	{
		if(this.StaticLengthLine == false)
        {
				this.UpdateLineStatic();
        }
        else
        {
             this.clear_x = this.player.x+11*this.player.width/32;
             this.clear_y = this.player.y + 7*this.player.height/8;
             this.UpdateStaticLengthStationary();
             this.counter ++;
        }
	}
	
	this.lineSection.setAll('checkWorldBounds',true);
	this.lineSection.setAll('outOfBoundsKill',true);
    
	if(this.lineColorChangeTimer > 0)
		this.lineColorChangeTimer -= this.time.elapsed;
	else
		this.lineColorChangeTimer = 0;
	
},

LaserHitLine: function (_laser, _line)
{
    
	if((_line.tint == 0x0021CCE5 && _laser.key == 'bLaser') || (_line.tint == 0xFFFF8000 && _laser.key == 'oLaser')){
    	_laser.destroy();
	}
	else{
		//_laser.destroy();
    	_line.destroy();
	}
},

MoveBackground: function (background) 
{
      if (background.y > this.screenHeight) 
      {
        background.y = -this.screenHeight;
        background.y += this.bgScrollSpeed;
      } 
    else {}
        background.y += this.bgScrollSpeed;
},
  
render: function ()
{
//	this.debug.spriteInfo(player,32,32);
},
    
ChangeLineSprite:function ()
{
	if(this.blueLineColor == true)
	{
		/*for(var i=1; i < numSections-1; i++)
		{
			var x = this.lineSection[i].x;
			var y = this.lineSection[i].y;
			this.lineSection[i].destroy();
			this.lineSection[i] = this.add.sprite(x,y,'redtrail');
			this.lineSection[i].anchor.setTo(0.5,0.5);
			this.physics.enable(this.lineSection[i],Phaser.Physics.ARCADE);
		}*/
		this.lineSection.setAll('tint',0x0021CCE5);
	}
	else
	{
		this.lineSection.setAll('tint',0xFFFF8000);
		/*for(var i=1; i < numSections-1; i++)
		{
			var x = this.lineSection[i].x;
			var y = this.lineSection[i].y;
			this.lineSection[i].destroy();
			this.lineSection[i] = this.add.sprite(x,y,'bluetrail');
			this.lineSection[i].anchor.setTo(0.5,0.5);
			this.physics.enable(this.lineSection[i],Phaser.Physics.ARCADE);
		}*/
	}
	/*var p_x = player.x;
	var p_y = player.y;
	this.lineSection[i].destroy();
	this.lineSection[i] = this.add.sprite(x,y,'bluetrail');
	this.lineSection[i].anchor.setTo(0.5,0.5);
	this.physics.enable(this.lineSection[i],Phaser.Physics.ARCADE);*/
	//*/
},
    
UpdateStaticLength: function ()
{
	//pop and destroy the last, add a new one in front
    if(this.lineSection.length >= this.numSegments)
	   this.lineSection.getBottom().destroy();
	
    this.lineSection.create(this.clear_x,this.clear_y,'trail');
	this.ChangeLineSprite();
},

UpdateStaticLengthStationary : function ()
{
     
    if(this.counter < this.numSegments+1) 
    {
        for(var i = 0; i < this.numSegments;i++)
        {
            this.lineSection.getAt(i).y += this.updateSpeed;
            
        }//*/
      //  this.lineSection.setAll('y',4,false,false,1,false); broken!
        this.lineSection.getBottom().destroy();
        this.lineSection.create(this.clear_x,this.clear_y,'trail');
        this.ChangeLineSprite();
       
    }
        /*//this.lineSection.setAll('y',4,false,false,1,false);
        this.lineSection.getBottom().destroy();
        this.lineSection.create(this.clear_x,this.clear_y,'trail');
        this.ChangeLineSprite(); */  
},
    
UpdateLineStatic: function ()
{
	/*var part = linePath.pop();
	part.setTo(this.clear_x,this.clear_y);
	linePath.unshift(part);
    for(var s in this.lineSection)
	{
	
	}*/
	this.lineSection.setAll('y',this.updateSpeed,false,false,1,false);	
	this.ChangeLineSprite();
},
    
UpdateLineLeftRight: function (i_direction)
{
	if(i_direction == 'r')
	{
		this.lineSection.setAll('x',-1,false,false,1,false);	
	}
	else
	{
		this.lineSection.setAll('x',1,false,false,1,false);
	}
	this.lineSection.create(this.player.x+11*this.player.width/32,this.player.y + 7*this.player.height/8,'trail');
	this.ChangeLineSprite();
	//this.lineSection.setAll('y',4,false,false,1);	
},
    
UpdateLineUpDown: function (i_direction)
{
		//	this.lineSection.setAll('y',4,false,false,1,false);
		//this.lineSection.setAll('y',1,false,false,1,false);	
	this.lineSection.create(this.player.x+11*this.player.width/32,this.player.y + 7*this.player.height/8,'trail');
	this.ChangeLineSprite();
	this.lineSection.setAll('y',1,false,false,1,false);	
},
    
UpdateEnemy: function ()
{
    
    for(var i=0; i<this.numLasers; i++){
        if(this.laser1[i].visible == false){
            if(b1 == 0){
				if((Math.floor((Math.random() * 50) + 1) % 2 == 0)){
                	this.laser1[i] = this.add.sprite(100, this.screenHeight-150, 'bLaser');
				}
				else{
					this.laser1[i] = this.add.sprite(100, this.screenHeight-150, 'oLaser');
				}
                this.physics.enable(this.laser1[i], Phaser.Physics.ARCADE);
                this.laser1[i].body.velocity.setTo(0, -this.laserSpeed);
                this.laser1[i].checkWorldBounds = true;
                this.laser1[i].outOfBoundsKill = true;
                b1 = Math.floor((Math.random() * 100) + 30);
            }
            else {
                b1 = b1-1;
            }
        }
    }
	
    for(var i=0; i<this.numLasers; i++){
	   if(this.laser2[i].visible == false){
            if(b2 == 0){
				if((Math.floor((Math.random() * 50) + 1) % 2 == 0)){
                	this.laser2[i] = this.add.sprite(260, this.screenHeight-150, 'bLaser');
                	this.laser2[i] = this.add.sprite(260, this.screenHeight-150, 'bLaser');
				}
				else{
					this.laser2[i] = this.add.sprite(260, this.screenHeight-150, 'oLaser');
				}
                this.physics.enable(this.laser2[i], Phaser.Physics.ARCADE);
                this.laser2[i].body.velocity.setTo(0, -this.laserSpeed);
                this.laser2[i].checkWorldBounds = true;
                this.laser2[i].outOfBoundsKill = true;
                b2 = Math.floor((Math.random() * 100) + 30);
            }
            else {
                b2 = b2-1;
            }   
        }
    }
    
    for(var i=0; i<this.numLasers; i++)
    {
        if(this.laser3[i].visible == false)
        {
            if(b3 == 0)
            {
				if((Math.floor((Math.random() * 50) + 1) % 2 == 0))
                {
                	this.laser3[i] = this.add.sprite(410, this.screenHeight-150, 'bLaser');
				}
				else
                {
					this.laser3[i] = this.add.sprite(410, this.screenHeight-150, 'oLaser');
				}
                this.physics.enable(this.laser3[i], Phaser.Physics.ARCADE);
                this.laser3[i].body.velocity.setTo(0, -this.laserSpeed);
                this.laser3[i].checkWorldBounds = true;
                this.laser3[i].outOfBoundsKill = true;
                b3 = Math.floor((Math.random() * 100) + 30);
            }
            else 
            {
                b3 = b3-1;
            }   
        }
    }
	
}
};