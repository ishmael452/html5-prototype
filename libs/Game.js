var screenHeight = 900;
var sceenWidth = 550;
var game = new Phaser.Game(sceenWidth,screenHeight,Phaser.AUTO,'', {preload: preload, create: create, update: update});

var player;
//var trail;
var cursors;
var isPressed = false;
var lineSection;
var linePath = new Array();
var spacer = 1;
var clear_x;
var clear_y;
var rotate_line;
var camera;
var blueLineColor = true;
var lineColorChangeTimer = 0;
var bg1;
var bg2;
var StaticLengthLine = true;
var numSegments = 128;
var speed = 512;
var counter;
var bgScrollSpeed = 7;

function preload()
{
    game.load.image('player','assets/character.png');
    game.load.image('bluetrail','assets/bluetrail.png');
	//game.load.image('redtrail','assets/redtrail.png');
	game.load.image('trail','assets/trail.png');
    game.load.image('background','assets/background.png');
}

function create()
{
    

    bg1 = this.game.add.sprite(0, 0, 'background');
    bg2 = this.game.add.sprite(0, -screenHeight, 'background');
    
	lineSection = game.add.group();
	
	ChangeLineSprite();//*/
   /* for(var i = 0; i < numSections*spacer; i++)
	{
		linePath[i] = new Phaser.Point(game.world.centerX,400);
	}//*/
	game.physics.enable(lineSection,Phaser.Physics.ARCADE);

    player = game.add.sprite(game.world.centerX,400,'player','character.png');
    game.physics.enable(player, Phaser.Physics.ARCADE);
   for(var i=0; i < numSegments; i++)
	{
		lineSection.create(player.x+11*player.width/32,player.y + 7*player.height/8,'trail');
	}
    player.body.collideWorldBounds = true;
    player.body.bounce.set(1);
    player.body.immovable = true;
    
   
    
    clear_x = player.x;
	clear_y = player.y;
	rotate_line = 0;
    cursors = game.input.keyboard.createCursorKeys();
    
 
    
}

function playerHitLine(_player,_line)
{
	//player.x = game.world.centerX;
	//player.y = 32;
	//_player.kill();
	//game.paused = true;
	//cursors = null;
}

function update() 
{

    
	if(game.paused == true)
	   return;
    //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
    //game.physics.arcade.overlap(player, stars, collectStar, null, this);

    //  Reset the players velocity (movement)
    
    moveBackground(bg1);
    moveBackground(bg2);
    
    player.body.velocity.setTo(0,0);
    //player.anchor.setTo(0.5,0.5);
    game.physics.arcade.enable(player);
   // game.physics.arcade.overlap(player,lineSection,playerHitLine,null,true);
    player.body.bounce.y = 0.2;
    player.body.gravity.y = 0;
    player.body.collideWorldBounds = true;
    

  
	if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && lineColorChangeTimer==0)
	{
		lineColorChangeTimer = 100;
		blueLineColor = !blueLineColor;
		ChangeLineSprite();
        
	}

	/*for(var i = 1; i < numSections-1; i++)
	{
		
		game.physics.arcade.collide(player,lineSection[i],playerHitLine,null,true);
	}*/
	//game.physics.arcade.collide(player,lineSection, playerHitLine,null,this);
    if (cursors.left.isDown)
    {
        //  Move to the left
        player.body.velocity.x = -speed;
		clear_x = player.x+11*player.width/32;
		clear_y = player.y + 7*player.height/8;
		rotate_line = 90;
        counter = 0;
		if(StaticLengthLine == false)
			UpdateLineLeftRight('l');
		else
			UpdateStaticLength();
       // player.animations.play('left');
    }
    else if (cursors.right.isDown)
    {
        //  Move to the right
        player.body.velocity.x = speed;
		clear_x = player.x+11*player.width/32;
		clear_y = player.y + 7*player.height/8;
		rotate_line = 90;
        counter = 0;
		if(StaticLengthLine == false)
			UpdateLineLeftRight('r');
		else
			UpdateStaticLength();
        //player.animations.play('right');
    }
    
    //  Allow the player to jump if they are touching the ground.
    else if (cursors.up.isDown)
    {
        player.body.velocity.y = -speed;
		clear_x = player.x+11*player.width/32;
		clear_y = player.y + 7*player.height/8;
		rotate_line = 180;
        counter = 0;
		if(StaticLengthLine == false)
			UpdateLineUpDown('u');
		else
			UpdateStaticLength();
    }
	else if (cursors.down.isDown)
	{
		player.body.velocity.y = speed;
		clear_x = player.x+11*player.width/32;
		clear_y = player.y + 7*player.height/8;
		rotate_line = 180;
        counter = 0;
		if(StaticLengthLine == false)
			UpdateLineUpDown('d');
		else
		{
			UpdateStaticLength();
		}
	}
	else
	{
		if(StaticLengthLine == false)
			{
				UpdateLineStatic();
			}
        else{
           
             clear_x = player.x+11*player.width/32;
             clear_y = player.y + 7*player.height/8;
             UpdateStaticLengthStationary();
             counter ++;
        }
	}
	
	lineSection.setAll('checkWorldBounds',true);
	lineSection.setAll('outOfBoundsKill',true);
	if(lineColorChangeTimer > 0)
		lineColorChangeTimer -= game.time.elapsed;
	else
		lineColorChangeTimer = 0;
	

}


function moveBackground(background) 
{
      if (background.y > screenHeight) 
      {
        background.y = -screenHeight;
        background.y += bgScrollSpeed;
      } 
    else {}
        background.y +=bgScrollSpeed;
}

  
function render()
{
	game.debug.spriteInfo(player,32,32);
}
function ChangeLineSprite()
{
	if(blueLineColor == true)
	{
		/*for(var i=1; i < numSections-1; i++)
		{
			var x = lineSection[i].x;
			var y = lineSection[i].y;
			lineSection[i].destroy();
			lineSection[i] = game.add.sprite(x,y,'redtrail');
			lineSection[i].anchor.setTo(0.5,0.5);
			game.physics.enable(lineSection[i],Phaser.Physics.ARCADE);
		}*/
		lineSection.setAll('tint',0x0021CCE5);
	}
	else
	{
		lineSection.setAll('tint',0xFFFF8000);
		/*for(var i=1; i < numSections-1; i++)
		{
			var x = lineSection[i].x;
			var y = lineSection[i].y;
			lineSection[i].destroy();
			lineSection[i] = game.add.sprite(x,y,'bluetrail');
			lineSection[i].anchor.setTo(0.5,0.5);
			game.physics.enable(lineSection[i],Phaser.Physics.ARCADE);
		}*/
	}
	/*var p_x = player.x;
	var p_y = player.y;
	lineSection[i].destroy();
	lineSection[i] = game.add.sprite(x,y,'bluetrail');
	lineSection[i].anchor.setTo(0.5,0.5);
	game.physics.enable(lineSection[i],Phaser.Physics.ARCADE);*/
	//*/
}
function UpdateStaticLength()
{
	//pop and destroy the last, add a new one in front
	lineSection.getBottom().destroy();
	lineSection.create(clear_x,clear_y,'trail');
	ChangeLineSprite();
}

function UpdateStaticLengthStationary()
{
    if(counter < numSegments) 
    {
        lineSection.setAll('y',4,false,false,1,false);
        lineSection.getBottom().destroy();
        lineSection.create(clear_x,clear_y,'trail');
        ChangeLineSprite();
        
    }
}
function UpdateLineStatic()
{
	/*var part = linePath.pop();
	part.setTo(clear_x,clear_y);
	linePath.unshift(part);
    for(var s in lineSection)
	{
	
	}*/
	lineSection.setAll('y',4,false,false,1);	
	ChangeLineSprite();
}
function UpdateLineLeftRight(i_direction)
{
	if(i_direction == 'r')
	{
		lineSection.setAll('x',-1,false,false,1,false);	
	}
	else
	{
		lineSection.setAll('x',1,false,false,1,false);
	}
	lineSection.create(player.x+11*player.width/32,player.y + 7*player.height/8,'trail');
	ChangeLineSprite();
	//lineSection.setAll('y',4,false,false,1);	
}
function UpdateLineUpDown(i_direction)
{
		//	lineSection.setAll('y',4,false,false,1,false);
		//lineSection.setAll('y',1,false,false,1,false);	
	lineSection.create(player.x+11*player.width/32,player.y + 7*player.height/8,'trail');
	ChangeLineSprite();
	lineSection.setAll('y',1,false,false,1);	
}